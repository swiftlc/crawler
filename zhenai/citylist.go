package zhenai

import (
    "crawler/distribute/config"
    "crawler/engine"
    "regexp"
)

const CityListRe = `<a.*?href="(http://www.zhenai.com/zhenghun/.*?)".*?>([^<]+?)<`

func ParseCityList(content []byte) engine.ParseResult {
    re := regexp.MustCompile(CityListRe)
    submatch := re.FindAllSubmatch(content, -1)
    var result engine.ParseResult
    for _, match := range submatch {
        //result.Items = append(result.Items, "City :"+string(match[2]))
        result.Requests = append(result.Requests, engine.Request{
            Url:    string(match[1]),
            Parser: engine.NewFuncParser(config.ZhenAiParseCity, ParseCity),
        })
        
    }
    
    return result
}
