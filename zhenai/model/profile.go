package model

type Profile struct {
    Name     string `json:"name"`
    Gender   string `json:"gender"`
    Location string `json:"location"`
    Height   int    `json:"height"`
    Age      int    `json:"age"`
    Marriage string `json:"marriage"`
    SalaryL  int    `json:"salaryL"`
    SalaryH  int    `json:"salaryH"`
    Edu      string `json:"edu"`
}
