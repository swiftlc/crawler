package main

import (
    "crawler/distribute/config"
    "crawler/engine"
    "crawler/persist"
    "crawler/scheduler"
    "crawler/zhenai"
    "log"
)

func main() {
    itemSaver, err := persist.CreateItemSaver()
    if err != nil {
        log.Fatalf("start item saver err %s", err.Error())
    }
    
    e := engine.ConcurrentEngine{
        Scheduler:        new(scheduler.QueuedScheduler),
        WorkCount:        100,
        ItemChan:         itemSaver,
        RequestProcessor: engine.Work,
    }
    e.Run(engine.Request{
        Url:    "http://www.zhenai.com/zhenghun",
        Parser: engine.NewFuncParser(config.ZhenAiParseCityList, zhenai.ParseCityList),
    })
}
