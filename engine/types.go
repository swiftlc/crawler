package engine

type ParseFunc func([]byte) ParseResult

type Parser interface {
    Parse([]byte) ParseResult
    Serialize() (name string)
}

type FuncParser struct {
    Name string
    Func ParseFunc
}

func NewFuncParser(name string, f ParseFunc) Parser {
    return &FuncParser{
        Name: name,
        Func: f,
    }
}

func (f *FuncParser) Parse(c []byte) ParseResult {
    return f.Func(c)
}

func (f *FuncParser) Serialize() (name string) {
    return f.Name
}

type Request struct {
    Url    string
    Parser Parser
}

type ParseResult struct {
    Requests []Request
    Items    []interface{}
}
