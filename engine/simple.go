package engine

type SimpleEngine struct {
    ItemChan chan interface{}
}

func (e *SimpleEngine) Run(seeds ...Request) {
    var requests []Request
    requests = append(requests, seeds...)
    
    for len(requests) > 0 {
        req := requests[0]
        
        parseResult := Work(req)
        
        requests = append(requests, parseResult.Requests...)
        
        for _, v := range parseResult.Items {
            e.ItemChan <- v
        }
        
        requests = requests[1:]
    }
}
