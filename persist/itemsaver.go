package persist

import (
    "context"
    "github.com/olivere/elastic/v7"
    "log"
)

func CreateItemSaver() (chan interface{}, error) {
    client, err := elastic.NewClient(elastic.SetURL("http://192.168.1.8:9200"))
    if err != nil {
        return nil, err
    }
    out := make(chan interface{})
    go func() {
        //itemCount := 0
        for item := range out {
            //itemCount++
            //log.Printf("Item Saver: got item #%d: %v", itemCount, item)
            Save(client, "zhenai", item)
        }
    }()
    return out, nil
}

//{
//	"mappings": {
//		"properties": {
//			"name": {
//				"type": "text",
//				"analyzer": "ik_max_word",
//				"search_analyzer": "ik_smart"
//			},
//			"gender": {
//				"type": "text",
//				"analyzer": "ik_max_word",
//				"search_analyzer": "ik_smart"
//			},
//			"location": {
//				"type": "text",
//				"analyzer": "ik_max_word",
//				"search_analyzer": "ik_smart"
//			},
//			"height": {
//				"type": "integer"
//			},
//			"age": {
//				"type": "integer"
//			},
//			"Marriage": {
//				"type": "text",
//				"analyzer": "ik_max_word",
//				"search_analyzer": "ik_smart"
//			},
//			"salaryL": {
//				"type": "integer"
//			},
//			"salaryH": {
//				"type": "integer"
//			},
//			"edu": {
//				"type": "text",
//				"analyzer": "ik_max_word",
//				"search_analyzer": "ik_smart"
//			}
//		}
//	}
//}

func Save(client *elastic.Client, index string, item interface{}) (id string, err error) {
    log.Printf("Item Saver: got item : %v", item)
    
    resp, err := client.Index().Index(index).BodyJson(item).Do(context.Background())
    
    bq := elastic.NewBoolQuery()
    bq.Must()
    client.Search("index").Query()
    
    if err != nil {
        return "", err
    }
    return resp.Id, err
}
