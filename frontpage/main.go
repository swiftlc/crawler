package main

import (
    "fmt"
    "github.com/gin-gonic/gin"
    "net/http"
)

func main() {
    router := gin.Default()
    router.LoadHTMLGlob("frontpage/templates/*")
    router.GET("/", func(ctx *gin.Context) {
        ctx.HTML(http.StatusOK, "index.html", nil)
    })
    router.POST("/search", func(ctx *gin.Context) {
        var mp map[string]string
        ctx.Bind(&mp)
        fmt.Println("request map:",mp)
        ctx.JSON(http.StatusOK, "hello")
    })
    
    router.Run(":8000")
}
