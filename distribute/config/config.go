package config

const (
    ItemSaverPort = 1234
    
    ProcessorPort = 9800
    
    ItemSaverRpc   = "ItemSaverService.Save"
    CrawlServerRpc = "CrawlService.Process"
    
    ElasticSearchIndex = "zhenai"
    
    ZhenAiParseCityList = "ZhenAiParseCityList"
    ZhenAiParseCity     = "ZhenAiParseCity"
)
