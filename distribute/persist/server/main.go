package main

import (
    "crawler/distribute/config"
    "crawler/distribute/persist"
    "crawler/distribute/rpcsupport"
    "flag"
    "fmt"
    "github.com/olivere/elastic/v7"
    "log"
)

var port = flag.Int("port", 1234, "server port to bind")

func main() {
    flag.Parse()
    client, err := elastic.NewClient(elastic.SetURL("http://192.168.1.8:9200"))
    if err != nil {
        panic(err)
    }
    log.Printf("Start server bind port %d", *port)
    rpcsupport.StartJsonRpcServer(fmt.Sprintf(":%d", *port), &persist.ItemSaverService{
        Client: client,
        Index:  config.ElasticSearchIndex,
    })
}
