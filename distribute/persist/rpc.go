package persist

import (
    "crawler/persist"
    "github.com/olivere/elastic/v7"
)

type ItemSaverService struct {
    Client *elastic.Client
    Index  string
}

func (s *ItemSaverService) Save(item interface{}, result *string) error {
    _, err := persist.Save(s.Client, s.Index, item)
    if err == nil {
        *result = "OK"
    }
    return err
}
