package client

import (
    "crawler/distribute/config"
    "crawler/distribute/rpcsupport"
    "log"
)

func CreateItemSaver(addr string) (chan interface{}, error) {
    client, err := rpcsupport.CreateJsonRpcClient(addr)
    if err != nil {
        return nil, err
    }
    
    out := make(chan interface{})
    go func() {
        for item := range out {
            var result string
            err := client.Call(config.ItemSaverRpc, item, &result)
            if err != nil {
                log.Printf("save item error :%s", err.Error())
            }
        }
    }()
    return out, nil
}
