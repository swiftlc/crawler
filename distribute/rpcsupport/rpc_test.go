package rpcsupport

import (
    "testing"
    "time"
)

type Math struct {
}

type Args struct {
    A int
    B int
}

func (m Math) Add(args Args, result *int) error {
    *result = args.A + args.B
    return nil
}

func TestCreateJsonRpcClient(t *testing.T) {
    go StartJsonRpcServer(":8001", Math{})
    
    time.Sleep(time.Second)
    
    client, err := CreateJsonRpcClient(":8001")
    if err != nil {
        t.Error(err)
    }
    defer client.Close()
    
    var ret int
    err = client.Call("Math.Add", Args{
        A: 50,
        B: 20,
    }, &ret)
    if err != nil {
        t.Error(err)
    }
    t.Logf("rpc call return %d", ret)
}
