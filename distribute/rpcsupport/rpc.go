package rpcsupport

import (
    "net"
    "net/rpc"
    "net/rpc/jsonrpc"
)

func StartJsonRpcServer(bind string, server interface{}) error {
    err := rpc.Register(server)
    if err != nil {
        return err
    }
    
    l, err := net.Listen("tcp", bind)
    if err != nil {
        return err
    }
    
    defer l.Close()
    
    for {
        conn, err := l.Accept()
        if err != nil {
            continue
        }
        
        go jsonrpc.ServeConn(conn)
    }
}

func CreateJsonRpcClient(addr string) (*rpc.Client, error) {
    return jsonrpc.Dial("tcp", addr)
}
