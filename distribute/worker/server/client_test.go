package main

import (
    "crawler/distribute/config"
    "crawler/distribute/rpcsupport"
    "crawler/distribute/worker"
    "crawler/engine"
    "crawler/zhenai"
    "testing"
)

func TestCrawlService(t *testing.T) {
    host := ":9000"
    go rpcsupport.StartJsonRpcServer(host, worker.CrawlService{})
    
    client, err := rpcsupport.CreateJsonRpcClient(host)
    if err != nil {
        t.Errorf("connect to crawlservice error:%s", err.Error())
    }
    defer client.Close()
    
    req := engine.Request{
        Url:    "http://www.zhenai.com/zhenghun/aba",
        Parser: engine.NewFuncParser(config.ZhenAiParseCity, zhenai.ParseCity),
    }
    
    serializedReq := worker.SerializeRequest(req)
    
    var serializedParseResult worker.ParseResult
    
    err = client.Call(config.CrawlServerRpc, serializedReq, &serializedParseResult)
    if err != nil {
        t.Errorf("call [%s] error:%s", config.CrawlServerRpc, err.Error())
    }
    
    t.Log(serializedParseResult)
}
