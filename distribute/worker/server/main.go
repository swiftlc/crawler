package main

import (
    "crawler/distribute/rpcsupport"
    "crawler/distribute/worker"
    "flag"
    "fmt"
    "log"
)

var port = flag.Int("port", 9000, "server port to listen")

func main() {
    flag.Parse()
    crawlService := &worker.CrawlService{}
    log.Printf("Start server bind port %d", *port)
    log.Fatal(rpcsupport.StartJsonRpcServer(fmt.Sprintf(":%d", *port), crawlService))
}
