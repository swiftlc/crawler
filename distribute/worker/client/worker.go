package client

import (
    "crawler/distribute/config"
    "crawler/distribute/worker"
    "crawler/engine"
    "log"
    "net/rpc"
)

func CreateProcessor(c chan *rpc.Client) (engine.Processor, error) {
    return func(req engine.Request) engine.ParseResult {
        serializedReq := worker.SerializeRequest(req)
        var serializedParseResult worker.ParseResult
        client := <-c
        err := client.Call(config.CrawlServerRpc, serializedReq, &serializedParseResult)
        if err != nil {
            log.Printf("call [%s] error :%s", config.CrawlServerRpc, err.Error())
            return engine.ParseResult{}
        }
        return worker.DeserializeParseResult(serializedParseResult)
    }, nil
}
