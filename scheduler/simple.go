package scheduler

import "crawler/engine"

type SimpleScheduler struct {
    workerChan engine.RequestChan
}

func (s *SimpleScheduler) Run() {
    s.workerChan = make(engine.RequestChan)
}

func (s *SimpleScheduler) WorkerChan() engine.RequestChan {
    return s.workerChan
}

func (s *SimpleScheduler) WorkerReady(engine.RequestChan) {
    //do nothing
}

func (s *SimpleScheduler) Submit(request engine.Request) {
    go func() { s.workerChan <- request }()
}
