module crawler

go 1.14

require (
	github.com/PuerkitoBio/goquery v1.5.1
	github.com/gin-gonic/gin v1.6.3 // indirect
	github.com/olivere/elastic/v7 v7.0.16
	golang.org/x/net v0.0.0-20200602114024-627f9648deb9
	golang.org/x/text v0.3.2
)
